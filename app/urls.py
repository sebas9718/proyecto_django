from django.urls import path 
from . import views 

app_name = 'app' 
urlpatterns = [
    path('', views.index, name='index'), 
    path('error_autenticar', views.error_autenticar, name='error_autenticar'), 
    path('autenticar/', views.autenticar, name='autenticar'),
    path('salir/', views.post_salir, name='post_salir'),

    path('crea_empresa/', views.crea_empresa, name='crea_empresa'),
    path('crea_empresa_post/', views.post_crea_empresa, name='post_crea_empresa'),

    path('estudiante/', views.estudiante, name='estudiante'),
    path('estudiante/lista_postulaciones/', views.lista_postulaciones, name='lista_postulaciones'),
    path('estudiante/consultar_postulacion/<int:postulacion_id>/', views.consultar_postulacion, name='consultar_postulacion'),
    path('estudiante/lista_convocatorias_abiertas/', views.lista_convocatorias_abiertas, name='lista_convocatorias_abiertas'),
    path('estudiante/consulta_convocatoria_abierta/<int:convocatoria_id>/', views.consulta_convocatoria_abierta, name='consulta_convocatoria_abierta'),
    path('estudiante/editar_perfil_estudiante/', views.editar_perfil_estudiante, name='editar_perfil_estudiante'),
    path('estudiante/editar_perfil_estudiante_post/', views.editar_perfil_estudiante_post, name='editar_perfil_estudiante_post'),
    path('estudiante/confirmar_postulacion/<int:convocatoria_id>/', views.confirmar_postulacion, name='confirmar_postulacion'),
    path('estudiante/confirmar_postulacion_post/<int:convocatoria_id>/', views.confirmar_postulacion_post, name='confirmar_postulacion_post'),
    path('estudiante/error_postulacion/', views.error_postulacion, name='error_postulacion'),
    
    path('empresa/', views.empresa, name='empresa'),
    path('empresa/crea_convocatoria/', views.crea_convocatoria, name='crea_convocatoria'),
    path('empresa/crea_convocatoria_post/', views.crea_convocatoria_post, name='crea_convocatoria_post'),
    path('empresa/lista_convocatoria/', views.lista_convocatoria, name='lista_convocatoria'),
    path('empresa/cerrar_convocatoria/<int:convocatoria_id>/', views.cerrar_convocatoria, name='cerrar_convocatoria'),
    path('empresa/cerrar_convocatoria_post/<int:convocatoria_id>/', views.cerrar_convocatoria_post, name='cerrar_convocatoria_post'),
    path('empresa/editar_convocatoria/<int:convocatoria_id>/', views.editar_convocatoria, name='editar_convocatoria'),
    path('empresa/editar_convocatoria_post/<int:convocatoria_id>/', views.editar_convocatoria_post, name='editar_convocatoria_post'),
    path('empresa/consulta_convocatoria/<int:convocatoria_id>/', views.consulta_convocatoria, name='consulta_convocatoria'),
    path('empresa/consulta_convocatoria/<int:convocatoria_id>/editar_postulacion/<int:postulacion_id>/', views.editar_postulacion, name='editar_postulacion'),
    path('empresa/consulta_convocatoria/<int:convocatoria_id>/editar_postulacion_post/<int:postulacion_id>/', views.editar_postulacion_post, name='editar_postulacion_post'),
    path('empresa/consulta_convocatoria/<int:convocatoria_id>/consulta_estudiante/<int:postulacion_id>/', views.consulta_estudiante, name='consulta_estudiante'),

    path('coordinador/', views.coordinador, name='coordinador'),
    path('coordinador/lista_empresas/', views.lista_empresas, name='lista_empresas'),
    path('coordinador/consultar_empresa/<int:empresa_id>/', views.consultar_empresa, name='consultar_empresa'),
    path('coordinador/lista_convocatorias_total/', views.lista_convocatorias_total, name='lista_convocatorias_total'),
    path('coordinador/consulta_convocatorias_total/<int:convocatoria_id>/', views.consulta_convocatorias_total, name='consulta_convocatorias_total'),
    path('coordinador/lista_estudiantes/', views.lista_estudiantes, name='lista_estudiantes'),
    path('coordinador/consultar_estudiante/<int:estudiante_id>/', views.consultar_estudiante, name='consultar_estudiante'),
    path('coordinador/crear_estudiante/', views.crear_estudiante, name='crear_estudiante'),
    path('coordinador/crear_estudiante_post/', views.crear_estudiante_post, name='crear_estudiante_post'),
    path('coordinador/consultar_historico/', views.consultar_historico, name='consultar_historico'),
    path('coordinador/actualizar_semestre/', views.actualizar_semestre, name='actualizar_semestre'),
    path('coordinador/actualizar_semestre_post/', views.actualizar_semestre_post, name='actualizar_semestre_post'),
]