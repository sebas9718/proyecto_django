from django.db import models
from django.contrib.auth.models import User

class Coordinador(models.Model):
    celular = models.CharField(max_length=45)
    user = models.ForeignKey(
        User,
        related_name='coordinadores',
        null=True,
        on_delete=models.PROTECT
    )

    class Meta():
        app_label = 'app'

class Estudiante(models.Model):
    fortalezas = models.TextField()
    herramientas = models.TextField()
    user = models.ForeignKey(
        User,
        related_name='estudiantes',
        null=True,
        on_delete=models.PROTECT
    )

    class Meta():
        app_label = 'app'

class Empresa(models.Model):
    razon_social = models.TextField()
    ciudad = models.CharField(max_length=45)
    nombre_contacto = models.CharField(max_length=45)
    fecha_creacion = models.DateField(auto_now_add=True)
    user = models.ForeignKey(
        User,
        related_name='empresas',
        null=True,
        on_delete=models.PROTECT
    )

    class Meta():
        app_label = 'app'

class Convocatoria(models.Model):
    nombre = models.CharField(max_length=45)
    descripcion = models.CharField(max_length=45)
    fecha_inicial = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
    fecha_final = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
    abierta = models.BooleanField(default=False)
    fecha_creacion = models.DateField(auto_now=False, auto_now_add=True)
    empresa = models.ForeignKey(
        Empresa,
        related_name='convocatorias',
        on_delete=models.PROTECT
    )

    class Meta():
        app_label = 'app'

class Postulacion(models.Model):
    semestre = models.CharField(max_length=45)
    aceptada = models.BooleanField(default=False)
    fecha_creacion = models.DateField(auto_now=False, auto_now_add=True)
    estudiante = models.ForeignKey(
        Estudiante,
        related_name='postulaciones',
        on_delete=models.PROTECT
    )
    convocatoria = models.ForeignKey(
        Convocatoria,
        related_name='postulaciones',
        on_delete=models.PROTECT
    )

    class Meta():
        app_label = 'app'
    
class Configuracion(models.Model):
    semestre = models.CharField(max_length=45)

    class Meta():
        app_label = 'app'