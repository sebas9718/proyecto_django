from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from app.models import Empresa, Coordinador, Estudiante, Convocatoria, Configuracion, Postulacion
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.db.models import Q
from django.contrib.auth.decorators import login_required

def index(request):
    return render(request, 'app/index.html')

def error_autenticar(request):
    return render(request, 'app/error_autenticacion.html')

def autenticar(request):
    usuarios = User.objects.all()
    usernames_usuarios = [u.username for u in usuarios]
    username = 'coordinador1'
    email = 'coordinador1@gmail.com'
    contraseña = '1234'
    if username not in usernames_usuarios:
        usuario1 = User(username= username, email= email)
        usuario1.set_password(contraseña)
        if usuario1 not in usuarios:
            usuario1.save()
        coor = Coordinador(celular='3502757520')
        coor.user = usuario1
        coordinadores = Coordinador.objects.all()
        if coor not in coordinadores:
            coor.save()
    username = request.POST['username']
    contraseña = request.POST['contra']
    usuario = authenticate(username=username, password=contraseña)
    if usuario is not None:
        login(request, usuario)
        #Verfica empresa
        try:
            empresa = Empresa.objects.get(user=usuario)
            return redirect('app:empresa')
        except:
            pass
        #Verifica estudiante
        try:
            estudiante = Estudiante.objects.get(user=usuario)
            return redirect('app:estudiante')
        except:
            pass
        #Verifica coordinador
        try:
            coordinador = Coordinador.objects.get(user=usuario)
            return redirect('app:coordinador')
        except:
            pass
    else:
        return redirect('app:error_autenticar') 

def post_salir(request):
    logout(request)
    return redirect('app:index')

def crea_empresa(request):
    return render(request, 'app/crea_empresa.html')

def post_crea_empresa(request):
    username = request.POST['username']
    contra = request.POST['contra']
    razon = request.POST['razon']
    ciudad = request.POST['ciudad']
    contacto = request.POST['contacto']
    email = request.POST['email']
    usuario = User(username=username, email=email)
    usuario.set_password(contra)
    usuario.save()
    emp = Empresa(razon_social=razon, ciudad=ciudad, nombre_contacto=contacto)
    emp.user = usuario
    emp.save()
    return redirect('app:index')

@login_required
def estudiante(request):
    return render(request, 'app/estudiante.html')

@login_required
def lista_postulaciones(request):
    estudiante = Estudiante.objects.get(user=request.user)
    postulaciones = Postulacion.objects.filter(estudiante=estudiante.id)
    contexto = {
        'postulaciones' : postulaciones
    }
    return render(request, 'app/lista_postulaciones.html', contexto)

@login_required
def consultar_postulacion(request, postulacion_id):
    estudiante = Estudiante.objects.get(user=request.user)
    postulaciones = Postulacion.objects.filter(estudiante=estudiante.id)
    postulacion = get_object_or_404(postulaciones, id=postulacion_id)
    contexto = {
        'postulacion' : postulacion
    }
    return render(request, 'app/consultar_postulacion.html', contexto)

@login_required
def lista_convocatorias_abiertas(request):
    convocatorias_abiertas = Convocatoria.objects.filter(Q(abierta=False))
    contexto = {
        'convocatorias' : convocatorias_abiertas
    }
    return render(request, 'app/lista_convocatorias_abiertas.html', contexto)

@login_required
def consulta_convocatoria_abierta(request, convocatoria_id):
    convocatorias = Convocatoria.objects.filter(Q(abierta=False))
    convocatoria = get_object_or_404(convocatorias, id=convocatoria_id)
    contexto = {
        'convocatoria' : convocatoria
    }
    return render(request, 'app/consulta_convocatoria_est.html', contexto)

@login_required
def editar_perfil_estudiante(request):
    estudiante = Estudiante.objects.get(user=request.user)
    contexto = {
        'estudiante' : estudiante
    }
    return render(request, 'app/editar_perfil_estudiante.html', contexto)

@login_required
def editar_perfil_estudiante_post(request):
    email = request.POST['email']
    fortalezas = request.POST['fortalezas']
    herramientas = request.POST['herramientas']
    estudiante = Estudiante.objects.get(user=request.user)
    estudiante.email = email
    estudiante.fortalezas = fortalezas
    estudiante.herramientas = herramientas
    estudiante.save()
    return redirect('app:estudiante')

@login_required
def confirmar_postulacion(request, convocatoria_id):
    convocatorias_abiertas = Convocatoria.objects.filter(Q(abierta=False))
    convocatoria = get_object_or_404(convocatorias_abiertas, id=convocatoria_id)
    contexto = {
        'convocatoria' : convocatoria
    }
    return render(request, 'app/confirmar_postulacion.html', contexto)

@login_required
def confirmar_postulacion_post(request, convocatoria_id):
    config = Configuracion.objects.get(id=1)
    convocatoria = Convocatoria.objects.get(id=convocatoria_id)
    estudiante = Estudiante.objects.get(user=request.user)
    postulaciones = Postulacion.objects.filter(estudiante=estudiante)
    id_convocatorias = []
    for pos in postulaciones:
        id_convocatorias.append(pos.convocatoria)
    postulacion = Postulacion(semestre=config.semestre)
    postulacion.estudiante = estudiante
    postulacion.convocatoria = convocatoria
    if postulacion.convocatoria not in id_convocatorias:
        postulacion.save()
        return redirect('app:lista_convocatorias_abiertas')
    else:
        return redirect('app:error_postulacion')

@login_required
def error_postulacion(request):
    return render(request, 'app/acceder_otro.html')

@login_required
def empresa(request):
    return render(request, 'app/empresa.html')

@login_required
def crea_convocatoria(request):
    return render(request, 'app/crea_convocatoria.html')

@login_required
def crea_convocatoria_post(request):
    nombre = request.POST['name']
    fecha_inicial = request.POST['fecha_inicial']
    fecha_final = request.POST['fecha_final']
    descripcion = request.POST['descripcion']
    convocatoria = Convocatoria(nombre=nombre, descripcion=descripcion, fecha_inicial=fecha_inicial, fecha_final=fecha_final)
    convocatoria.empresa = Empresa.objects.get(user=request.user)
    convocatoria.save()
    return redirect('app:empresa')

@login_required
def lista_convocatoria(request):
    empresa = Empresa.objects.get(user=request.user)
    convocatorias = Convocatoria.objects.filter(empresa=empresa.id)
    contexto = {
        'convocatorias' : convocatorias
    }
    return render(request, 'app/lista_convocatoria.html', contexto)

@login_required
def cerrar_convocatoria(request, convocatoria_id):
    empresa = Empresa.objects.get(user=request.user)
    convocatorias = Convocatoria.objects.filter(empresa=empresa.id)
    convocatoria = get_object_or_404(convocatorias, id=convocatoria_id)
    contexto = {
        'convocatoria' : convocatoria
    }
    return render(request, 'app/cerrar_convocatoria.html', contexto)

@login_required
def cerrar_convocatoria_post(request, convocatoria_id):
    convocatoria = Convocatoria.objects.get(id=convocatoria_id)
    convocatoria.abierta = True
    convocatoria.save()
    return redirect('app:lista_convocatoria')

@login_required
def editar_convocatoria(request, convocatoria_id):
    empresa = Empresa.objects.get(user=request.user)
    convocatorias = Convocatoria.objects.filter(empresa=empresa.id)
    convocatoria = get_object_or_404(convocatorias, id=convocatoria_id)
    contexto = {
        'convocatoria' : convocatoria
    }
    return render(request, 'app/editar_convocatoria.html', contexto)

@login_required
def editar_convocatoria_post(request, convocatoria_id):
    nombre = request.POST['name']
    fecha_inicial = request.POST['fecha_inicial']
    fecha_final = request.POST['fecha_final']
    estado = request.POST['esta']
    convocatoria = Convocatoria.objects.get(id=convocatoria_id)
    convocatoria.nombre = nombre
    convocatoria.fecha_inicial = fecha_inicial
    convocatoria.fecha_final = fecha_final
    if estado == 'cerrado':
        convocatoria.abierta = True
    else:
        convocatoria.abierta = False
    convocatoria.save()
    return redirect('app:lista_convocatoria')

@login_required
def consulta_convocatoria(request, convocatoria_id):
    empresa = Empresa.objects.get(user=request.user)
    convocatorias = Convocatoria.objects.filter(empresa=empresa.id)
    convocatoria = get_object_or_404(convocatorias, id=convocatoria_id)
    postulaciones = convocatoria.postulaciones.all() 
    contexto = {
        'convocatoria' : convocatoria,
        'postulaciones' : postulaciones
    }
    return render(request, 'app/consulta_convocatoria.html', contexto)

@login_required
def editar_postulacion(request, convocatoria_id, postulacion_id):
    empresa = Empresa.objects.get(user=request.user)
    convocatorias = Convocatoria.objects.filter(empresa=empresa.id)
    convocatoria = get_object_or_404(convocatorias, id=convocatoria_id)
    postulaciones = Postulacion.objects.filter(convocatoria=convocatoria)
    postulacion = get_object_or_404(postulaciones, id=postulacion_id)
    contexto = {
        'convocatoria' : convocatoria,
        'postulacion' : postulacion
    }
    return render(request, 'app/editar_postulacion.html', contexto)

@login_required
def editar_postulacion_post(request, convocatoria_id, postulacion_id):
    postulacion = Postulacion.objects.get(id=postulacion_id)
    estado = request.POST['esta']
    if estado == 'cerrado':
        postulacion.aceptada = False
    else:
        postulacion.aceptada = True
    postulacion.save()
    return redirect('app:lista_convocatoria')

@login_required
def consulta_estudiante(request, convocatoria_id, postulacion_id):
    empresa = Empresa.objects.get(user=request.user)
    convocatorias = Convocatoria.objects.filter(empresa=empresa.id)
    convocatoria = get_object_or_404(convocatorias, id=convocatoria_id)
    postulaciones = Postulacion.objects.filter(convocatoria=convocatoria)
    postulacion = get_object_or_404(postulaciones, id=postulacion_id)
    contexto = {
        'convocatoria' : convocatoria,
        'postulacion' : postulacion
    }
    return render(request, 'app/consulta_estudiante.html', contexto)

@login_required
def coordinador(request):
    return render(request, 'app/coordinador.html')

@login_required
def lista_empresas(request):
    empresas = Empresa.objects.all()
    contexto = {
        'lista_empresas' : empresas
    }
    return render(request, 'app/lista_empresas.html', contexto)

@login_required
def consultar_empresa(request, empresa_id):
    empresas = Empresa.objects.all()
    empresa = get_object_or_404(empresas, id=empresa_id)
    convocatorias = empresa.convocatorias.all()
    contexto = {
        'empresa' : empresa,
        'convocatorias' : convocatorias
    }
    return render(request, 'app/consultar_empresa.html', contexto)

@login_required
def lista_convocatorias_total(request):
    convocatorias = Convocatoria.objects.all()
    contexto = {
        'convocatorias' : convocatorias
    }
    return render(request, 'app/lista_convocatorias_total.html', contexto)

@login_required
def consulta_convocatorias_total(request, convocatoria_id):
    convocatorias = Convocatoria.objects.all()
    convocatoria = get_object_or_404(convocatorias, id=convocatoria_id)
    postulaciones = convocatoria.postulaciones.all() 
    contexto = {
        'convocatoria' : convocatoria,
        'postulaciones' : postulaciones
    }
    return render(request, 'app/consulta_convocatoria_coo.html', contexto)

@login_required
def lista_estudiantes(request):
    estudiantes = Estudiante.objects.all()
    estado_estudiantes = []
    estado_estudiante = False
    for est in estudiantes:
        postulaciones = est.postulaciones.all()
        for pos in postulaciones:
            if pos.aceptada is True:
                estado_estudiante = True
                break
            else:
                estado_estudiante = False
        estado_estudiantes.append(estado_estudiante)
    contexto = {
        'estudiantes' : estudiantes,
        'estado_estudiantes' : estado_estudiantes
    }
    return render(request, 'app/lista_estudiantes.html', contexto)

@login_required
def consultar_estudiante(request, estudiante_id):
    estudiantes = Estudiante.objects.all()
    estudiante = get_object_or_404(estudiantes, id=estudiante_id)
    contexto = {
        'estudiante' : estudiante
    }
    return render(request, 'app/consulta_estudiante_coo.html', contexto)

@login_required
def crear_estudiante(request):
    return render(request, 'app/crear_estudiante.html')

@login_required
def crear_estudiante_post(request):
    username = request.POST['name']
    contraseña = request.POST['contra']
    nombres = request.POST['names']
    apellidos = request.POST['apellidos']
    email = request.POST['email']
    usuario = User(username=username, first_name=nombres, last_name=apellidos, email=email)
    usuario.set_password(contraseña)
    usuario.save()
    est = Estudiante()
    est.user = usuario
    est.save()
    return redirect('app:coordinador')

@login_required
def consultar_historico(request):
    postulaciones = Postulacion.objects.filter(Q(aceptada=True))
    contexto = {
        'postulaciones' : postulaciones
    }
    return render(request, 'app/consultar_historico.html', contexto)

@login_required
def actualizar_semestre(request):
    configs = Configuracion.objects.all()
    id_semestres = []
    for con in configs:
        id_semestres.append(con.id)
    semestre = '2020-2'
    id_semestre = 1
    if id_semestre not in id_semestres:
        config = Configuracion(id=id_semestre, semestre=semestre)
    else:
        config = Configuracion.objects.get(id=id_semestre)
    contexto = {
        'configuracion' : config
    }
    return render(request, 'app/actualizar_semestre.html', contexto)

@login_required
def actualizar_semestre_post(request):
    semestre = request.POST['semestre']
    config = Configuracion.objects.get(id=1)
    config.semestre = semestre
    config.save()
    return redirect('app:coordinador')